void handlePuzzle(String puzzle) {
  if (!animationPlaying) {
    if (puzzle.equals("liberty")) {
      animationPlaying = true;

      liberty = true;
      OscMessage confirm = new OscMessage("/confirm");
      confirm.add(puzzle);
      oscP5.send(confirm, table);
      OscMessage animate = new OscMessage("/animate");
      animate.add(puzzle);
      oscP5.send(animate, sar);
      oscP5.send(animate, std);
    } else if (puzzle.equals("equity")) {
      animationPlaying = true;

      equity = true;
      OscMessage confirm = new OscMessage("/confirm");
      confirm.add(puzzle);
      oscP5.send(confirm, table);
      OscMessage animate = new OscMessage("/animate");
      animate.add(puzzle);
      oscP5.send(animate, sar);
      oscP5.send(animate, std);
    } else if (puzzle.equals("brotherhood")) {
      animationPlaying = true;

      brotherhood = true;
      OscMessage confirm = new OscMessage("/confirm");
      confirm.add(puzzle);
      oscP5.send(confirm, table);
      OscMessage animate = new OscMessage("/animate");
      animate.add(puzzle);
      oscP5.send(animate, sar);
      oscP5.send(animate, std);
    }
  } else if (millis() > animationTime) {
    animationPlaying = false;
    animationTime = -1;
  }
}


public void send() {
  String ip = cp5.get(Textfield.class, "ip").getText();
  int port = Integer.parseInt(cp5.get(Textfield.class, "port").getText());
  String address = cp5.get(Textfield.class, "address").getText();
  String string = cp5.get(Textfield.class, "string").getText();

  if (port > 0 && !ip.isEmpty() && !address.isEmpty() && !string.isEmpty()) {
    println("Trying to send " + address +
      " with a value of \"" + string + "\"" +
      " to " + ip + ":" + port);

    NetAddress netAddress = new NetAddress(ip, port);
    OscMessage m = new OscMessage(address);
    m.add(string);
    oscP5.send(m, netAddress);
  } else {
    println("All fields must contain a value");
  }
}
