# Óleo de las Ideas  

Trabajo práctico 5 para Lenguaje Multimedial III. Año 2018.

## Módulos

El trabajo práctico se encuentra dividido en diversos módulos:

- [Mesa](https://gitlab.com/ianmethyst/oleo-table): Utilizando reacTIVision, el sketch puede saber la posición de las semiesferas que están colocadas sobre la mesa. Es la parte de la obra con la que se interactúa (entrada). El repositorio también incluye un sketch de Arduino que controla los LEDs indicadores de estado.
- [STD](https://gitlab.com/ianmethyst/oleo-std): Es el módulo que se ejecuta en los celulares quienes hagan uso de la instalación. Dibuja al Eidolon 3D sobre el feed de la cámara. Consiste de un cliente y un servidor que recibe información a través de OSC y la reenvía a todos los clientes conectados a través de WebSockets.
- SAR: Junto con el módulo STD, es el único módulo que da una salida visual, aunque esta vez a través de un proyector. Dibuja sobre las diferentes esculturas y permite la corrección de trapecio.

![Diagrama de módulos de Óleo de las Ideas](docs/modules.png)

Además, hay un repositorio adicional para la documentación del Trabajo Práctico que se encuentra en el siguiente repositorio: [oleo-documento](https://gitlab.com/ianmethyst/oleo-documento)


## ¿Cómo ejecutar?

En primer lugar, se debe clonar el repositorio con sus submódulos:

```shell
$ git clone --recurse-submodules https://gitlab.com/ianmethyst/oleo/
```

Para compilar reacTIVision e instalar las dependencias de NPM, se puede usar el script que se encuentra la raíz del repositorio:

```shell
$ ./install
```

## ¿Cómo usar?

Todos los módulos se pueden lanzar desde el sketch que se encuentra en la raíz de éste directorio, pero de momento éste sólo funciona en Linux (y probablemente en otros sistemas operativos compatibles con Unix). En el caso de estar usando otro sistema operativo, todas las partes se pueden correr de manera manual y aún deberían funcionar.



