void addOSCTester(PVector position) {
  cp5.addGroup("tester")
    .setLabel("OSC Tester")
    .setPosition(position.x, position.y)
    .setBackgroundHeight(54)
    .setWidth(350)
    .disableCollapse()
    .setBackgroundColor(GRAY_COLOR);

  cp5.addTextfield("ip")
    .setPosition(1, 15)
    .setSize(100, 20)
    .setValue(tableIP)
    .setGroup("tester");

  cp5.addTextfield("port")
    .setPosition(111, 15)
    .setSize(40, 20)
    .setValue(Integer.toString(stdPort))
    .setGroup("tester");

  cp5.addTextfield("address")
    .setPosition(161, 15)
    .setSize(50, 20)
    .setValue(testAddress)
    .setGroup("tester");

  cp5.addTextfield("string")
    .setPosition(221, 15)
    .setSize(50, 20)
    .setValue("test")
    .setGroup("tester");

  cp5.addButton("send")
    .setPosition(281, 15)
    .setGroup("tester");
}

void addResetButton() {
  cp5.addButton("reset")
    .setPosition(width / 2 - 35, 20)
    .setLabel("reset");
}

void addLaunchers(PVector position) {
  cp5.addGroup("launch")
    .setLabel("Launch")
    .setPosition(position.x, position.y)
    .setBackgroundHeight(84)
    .setWidth(309)
    .disableCollapse()
    .setBackgroundColor(GRAY_COLOR);

  cp5.addButton("execTable")
    .setPosition(0, 15)
    .setLabel("Table")
    .setGroup("launch");

  cp5.addButton("execReactivision")
    .setPosition(80, 15)
    .setLabel("reacTIVision")
    .setGroup("launch");

  cp5.addButton("execSTD")
    .setPosition(160, 15)
    .setLabel("STD")
    .setGroup("launch");

  cp5.addButton("execSAR")
    .setPosition(240, 15)
    .setLabel("SAR")
    .setGroup("launch");

  cp5.addButton("execAll")
    .setPosition(240 / 2, 50)
    .setLabel("All")
    .setGroup("launch");

  //  cp5.addButton("killTable")
  //    .setPosition(120, 250);
}

void addKillers(PVector position) {
  cp5.addGroup("kill")
    .setLabel("Kill")
    .setPosition(position.x, position.y)
    .setBackgroundHeight(84)
    .setWidth(309)
    .disableCollapse()
    .setBackgroundColor(GRAY_COLOR);

  cp5.addButton("killTable")
    .setPosition(0, 15)
    .setLabel("Table")
    .setValue(0)
    .setGroup("kill");

  cp5.addButton("killReactivision")
    .setPosition(80, 15)
    .setLabel("reacTIVision")
    .setValue(0)
    .setGroup("kill");

  cp5.addButton("killStd")
    .setPosition(160, 15)
    .setLabel("STD")
    .setValue(0)
    .setGroup("kill");

  cp5.addButton("killSAR")
    .setPosition(240, 15)
    .setLabel("SAR")
    .setValue(0)
    .setGroup("kill");

  cp5.addButton("killAll")
    .setPosition(240 / 2, 50)
    .setLabel("All")
    .setValue(0)
    .setGroup("kill");
}

void drawLEDs() {
  int center = width / 2;
  float offset = LED_SIZE * 1.25;

  if ( liberty  == true) { 
    fill(LIBERTY_COLOR);
  } else {
    fill(GRAY_COLOR);
  }
  ellipse(center - offset, offset + LED_SIZE /2, LED_SIZE, LED_SIZE);


  if (equity == true) { 
    fill(EQUITY_COLOR);
  } else {
    fill(GRAY_COLOR);
  }
  ellipse(center, offset + LED_SIZE /2, LED_SIZE, LED_SIZE);


  if ( brotherhood == true) { 
    fill(BROTHERHOOD_COLOR);
  } else {
    fill(GRAY_COLOR);
  }
  ellipse(center + offset, offset + LED_SIZE /2, LED_SIZE, LED_SIZE);
}
