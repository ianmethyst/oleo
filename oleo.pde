import netP5.*; 
import oscP5.*;
import controlP5.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.io.FileReader;

ControlP5 cp5;

OscP5 oscP5;
NetAddress table, sar, std;

int animationTime;
boolean animationPlaying;
boolean finalePlayed;

final color LIBERTY_COLOR = color(0, 85, 164);
final color EQUITY_COLOR = color (255, 255, 255);
final color BROTHERHOOD_COLOR = color(239, 65, 53);
final color GRAY_COLOR = color(50);
final int LED_SIZE = 50;

static final String tableIP = "127.0.0.1";
static final int tablePort = 3333;

static final String sarIP = "127.0.0.1";
static final int sarPort = 6666;

static final String stdIP = "127.0.0.1";
static final int stdPort = 5555;

static final String testAddress = "/test";

boolean liberty, equity, brotherhood;

void setup() {
  size(400, 460);
  oscP5 = new OscP5(this, 12000);

  table = new NetAddress(tableIP, tablePort);
  sar = new NetAddress(sarIP, sarPort);
  std = new NetAddress(stdIP, stdPort);

  cp5 = new ControlP5(this);

  int OSCTesterWidth = 350;
  int launcherWidth = 309;

  addOSCTester(new PVector(width - OSCTesterWidth - (width - OSCTesterWidth) / 2, 150));
  addLaunchers(new PVector(width - launcherWidth - (width - launcherWidth) / 2, 230));
  addKillers(new PVector(width - launcherWidth - (width - launcherWidth) / 2, 340));
  addResetButton();

  oscP5.plug(this, "handlePuzzle", "/puzzle", "s");
}

void draw() {
  background(0);
  drawLEDs();

  if (!animationPlaying && liberty && equity && brotherhood) {
    println("playing finale");
    animationTime = millis() + 37 * 1000;
    finalePlayed = true;
    OscMessage finale = new OscMessage("/animate");
    finale.add("finale");
    oscP5.send(finale, std);
    oscP5.send(finale, sar);
  } else if (finalePlayed && millis() > animationTime) {
    reset();
  }
}

public void reset() {
  liberty = equity = brotherhood = false;

  OscMessage m = new OscMessage("/reset");
  m.add(1);
  oscP5.send(m, table);
}

void oscEvent(OscMessage m) {
}

void controlEvent(ControlEvent e) {
  String button = e.getController().getName();
  float value = e.getController().getValue();
  boolean breakable = true;

  if (value != 0) {
    switch (button) {
    case "execAll":
      breakable = false;
    case "execTable":
      execute("table");
      if (breakable) {
        break;
      }
    case "execReactivision": 
      execute("reactivision");
      if (breakable) {
        break;
      }
    case "execSTD":
      println("executing std");
      execute("std");
      if (breakable) {
        break;
      }
    case "execSAR":
      execute("sar");
      if (breakable) {
        break;
      }
    }
  } else {
    switch (button) {
    case "killAll":
      breakable = false;
    case "killTable":
      kill("table");
      if (breakable) {
        break;
      }
    case "killReactivision": 
      kill("reactivision");
      if (breakable) {
        break;
      }
    case "killSTD":
      kill("std");
      if (breakable) {
        break;
      }
    case "killSAR":
      kill("sar");
      if (breakable) {
        break;
      }
    }
  }
}

void execute (String arg) {
  String sketchPath = sketchPath("");

  try {
    ProcessBuilder pb = new ProcessBuilder("bash", sketchPath + "/open", arg, sketchPath);
    pb.start();
  }
  catch(IOException ie) {
    ie.printStackTrace();
  }
}

void kill(String arg) {
  String sketchPath = sketchPath("");

  try {
    ProcessBuilder pb = new ProcessBuilder("bash", sketchPath + "/kill", arg, sketchPath);
    pb.start();
  }
  catch(IOException ie) {
    ie.printStackTrace();
  }
}
